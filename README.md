# README #

PrimeFinder is an app that users different sieves to find prime numbers from 1 to N and to find the Nth prime number

### Description ###

Prime Finder has 4 different sieve algorithms which can be used to find the prime numbers list upto N or to find the Nth prime.

It Uses the below sieves

1.  Sieve Of Atkins
2.  Sieve Of Atkins Optimized (Credit:  http://programmingpraxis.com/2010/02/19/sieve-of-atkin-improved/ )
3.  Sieve Of Eratosthenes
4.  Sieve Of Sundaram

User can select any sieve and generate prime numbers and compare the performance.
User can also switch off sieve selection and use default sieve (Atkins Optimized) and reuse previous results to increase performance. 

Once primes calculated for a higher number, the application saves the generated prime numbers so that they are not recomputed on similar queries.
For example if the app calculates prime numbers till 500, for further requests for prime where N<500 or if the nth prime requested is below 500, 
the app will reuse the already generated prime numbers and will not re calculate.

Did not use any third party libraries 

The app works on both tablets and phones in both landscape and portrait modes. Used Fragments to pickup different layout when run on a tablet.
minSdkVersion 16
 
### Running from source (Android Studio) ###

1. Download the project from BitBucket

2. Select "Open Existing Project" and Run on an android tablet or phone, device or emmulator.

### Testing ###

ToBeAdded
