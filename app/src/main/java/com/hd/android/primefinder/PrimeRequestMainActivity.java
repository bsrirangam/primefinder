package com.hd.android.primefinder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hd.android.primefinder.util.InputFilterMinMax;
import com.hd.android.primefinder.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of PrimeResults. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PrimeRequestDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class PrimeRequestMainActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private EditText primeListEditText;
    private Button primeListSubmitButton;

    private EditText nthPrimeEditText;
    private Button nthPrimeSubmitButton;

    private Spinner sieveSelector;
    private Switch sieve_switch;

    private int sieveSelected = 1;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primerequest_main);

        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        primeListSubmitButton = (Button) findViewById(R.id.prime_upper_bound_submit_button);
        primeListEditText = (EditText) findViewById(R.id.prime_upper_bound_edit_text);

        primeListEditText.setFilters(new InputFilter[]{new InputFilterMinMax("1", "2147483645")});

        nthPrimeSubmitButton = (Button) findViewById(R.id.nth_prime_index_submit_button);
        nthPrimeEditText = (EditText) findViewById(R.id.nth_prime_index_edit_text);

        nthPrimeEditText.setFilters(new InputFilter[]{new InputFilterMinMax("1", "2147483645")});

        sieveSelector = (Spinner) findViewById(R.id.spinner);
        sieve_switch = (Switch) findViewById(R.id.sieve_switch);

        List<String> list = new ArrayList<String>();
        list.add(getString(R.string.sieve_of_atkins));
        list.add(getString(R.string.sieve_of_atkins_optimized));
        list.add(getString(R.string.sieve_of_eratosthenes));
        list.add(getString(R.string.sieve_of_sundaram));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, list);

        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        sieveSelector.setAdapter(dataAdapter);
        sieveSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sieveSelected = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sieveSelector.setEnabled(false);

        sieve_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton cb, boolean on) {
                if (on) {
                    Toast.makeText(context, R.string.sieve_toast_on, Toast.LENGTH_LONG).show();
                    sieveSelector.setEnabled(true);
                } else {
                    Toast.makeText(context, R.string.sieve_toast_off, Toast.LENGTH_LONG).show();
                    sieveSelector.setEnabled(true);
                    sieveSelector.setEnabled(false);
                }
            }
        });

        if (findViewById(R.id.primerequest_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        primeListEditText.setOnEditorActionListener(setEditorAction(primeListSubmitButton));
        nthPrimeEditText.setOnEditorActionListener(setEditorAction(nthPrimeSubmitButton));
        primeListSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (primeListEditText.getText().length() > 0) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(primeListSubmitButton.getWindowToken(), 0);
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putInt(PrimeRequestDetailFragment.PRIME_LIST_ID, Integer.parseInt(primeListEditText.getText().toString()));
                        if (sieveSelector.isEnabled()) {
                            arguments.putInt(PrimeRequestDetailFragment.SIEVE_SELECTED, sieveSelected);
                        } else {
                            arguments.putInt(PrimeRequestDetailFragment.SIEVE_SELECTED, 0);
                        }
                        PrimeRequestDetailFragment fragment = new PrimeRequestDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.primerequest_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, PrimeRequestDetailActivity.class);
                        intent.putExtra(PrimeRequestDetailFragment.PRIME_LIST_ID, Integer.parseInt(primeListEditText.getText().toString()));
                        if (sieveSelector.isEnabled()) {
                            intent.putExtra(PrimeRequestDetailFragment.SIEVE_SELECTED, sieveSelected);
                        } else {
                            intent.putExtra(PrimeRequestDetailFragment.SIEVE_SELECTED, 0);
                        }
                        context.startActivity(intent);
                    }
                } else {
                    Utils.showAllert(getString(R.string.input_error), context);
                }
            }
        });

        nthPrimeSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nthPrimeEditText.getText().length() > 0) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(nthPrimeSubmitButton.getWindowToken(), 0);
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putInt(PrimeRequestDetailFragment.NTH_PRIME_ID, Integer.parseInt(nthPrimeEditText.getText().toString()));
                        if (sieveSelector.isEnabled()) {
                            arguments.putInt(PrimeRequestDetailFragment.SIEVE_SELECTED, sieveSelected);
                        } else {
                            arguments.putInt(PrimeRequestDetailFragment.SIEVE_SELECTED, 0);
                        }
                        PrimeRequestDetailFragment fragment = new PrimeRequestDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.primerequest_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, PrimeRequestDetailActivity.class);
                        intent.putExtra(PrimeRequestDetailFragment.NTH_PRIME_ID, Integer.parseInt(nthPrimeEditText.getText().toString()));
                        if (sieveSelector.isEnabled()) {
                            intent.putExtra(PrimeRequestDetailFragment.SIEVE_SELECTED, sieveSelected);
                        } else {
                            intent.putExtra(PrimeRequestDetailFragment.SIEVE_SELECTED, 0);
                        }
                        context.startActivity(intent);
                    }
                } else {
                    Utils.showAllert(getString(R.string.input_error), context);
                }

            }
        });

    }

    private TextView.OnEditorActionListener setEditorAction(final Button submitButton) {
        return new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submitButton.callOnClick();
                    return true;
                }
                return false;
            }
        };
    }


}
