package com.hd.android.primefinder.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by nsrirangam on 7/24/16.
 */
public class PrimeCalculator {
    public static List<Integer> listPrime = new ArrayList<>();
    public static int highestPrimeSearchSoFar = 3;
    public static List<Boolean> isPrime;

    public static int findPrimeTillN(int upToNumber, int sieve) {
        // reset results if sieve is selected
        if(sieve!=0){
            highestPrimeSearchSoFar = 3;
            listPrime = new ArrayList<>();
        }
        //if sieve is not selected or Upper bound is 1,2 or 3 or
        // if the primes numbers are not generated use one of the sieves to generate primes
        if (upToNumber != 1 && upToNumber != 2 && upToNumber != 3 && (upToNumber > highestPrimeSearchSoFar || sieve!=0)) {
            int resultSize = 0;
            highestPrimeSearchSoFar = 3;
            try {
                switch (sieve) {
                    case 1:
                        resultSize = findPrimeUsingSieveOfAtkins(upToNumber);
                        break;
                    case 0:
                    case 2:
                        // SieveOfAtkinsOptimized is our default generator
                        resultSize = findPrimeUsingSieveOfAtkinsOptimized(upToNumber);
                        break;
                    case 3:
                        resultSize = findPrimeUsingSieveOfEratosthenes(upToNumber);
                        break;
                    case 4:
                        resultSize = findPrimeUsingSieveOfSundaram(upToNumber);
                        break;
                }
            } catch (OutOfMemoryError error) {
                return -1;
            }
            highestPrimeSearchSoFar = upToNumber;
            return resultSize + 2;
        }
        if (upToNumber == 1 || upToNumber == 2 || upToNumber == 3) {
            if (listPrime == null || listPrime.size() < 3) {
                listPrime = new ArrayList<>();
                listPrime.add(2);
                listPrime.add(3);

            }
            return upToNumber - 1;
        } else {
            int location = Collections.binarySearch(listPrime, upToNumber);
            if (location < 0) {
                return Math.abs(location) - 1;
            } else {
                return location + 1;
            }
        }
    }

    public static int findNthPrime(int index, int sieve) {
        int upperBound;
        if (listPrime != null && listPrime.size() > index) {
            return listPrime.get(index);
        } else {
            if (index == 3) {
                upperBound = 5;
            } else {
                upperBound = (int) (index * (Math.log(index) / Math.log(2)));
            }
            try {
                switch (sieve) {
                    case 1:
                        findPrimeUsingSieveOfAtkins(upperBound);
                        break;
                    case 2:
                        findPrimeUsingSieveOfAtkinsOptimized(upperBound);
                        break;
                    case 3:
                        findPrimeUsingSieveOfEratosthenes(upperBound);
                        break;
                    case 4:
                        findPrimeUsingSieveOfSundaram(upperBound);
                        break;
                }
            } catch (OutOfMemoryError error) {
                return -1;
            }
            if(index<listPrime.size()) {
                return listPrime.get(index - 1);
            } else{
                return -1;
            }
        }
    }

//    private static int FindPrimeUsingBruteForce(int topCandidate) {
//        int totalCount = 1;
//        boolean isPrime = true;
//        for (long i = 3; i < topCandidate; i += 2) {
//            for (int j = 3; j * j <= i; j += 2) {
//                if ((i % j) == 0) {
//                    isPrime = false;
//                    break;
//                }
//            }
//            if (isPrime) {
//                totalCount++;
//            } else isPrime = true;
//        }
//        return totalCount;
//    }

    public static int findPrimeUsingSieveOfAtkins(int uptoNumber) {
        int totalCount = 0;


        isPrime = new ArrayList<Boolean>(Arrays.asList(new Boolean[uptoNumber + 1]));
        listPrime = new ArrayList<>();
        listPrime.add(2);
        listPrime.add(3);
        Collections.fill(isPrime, Boolean.FALSE);

        int squareRoot = (int) Math.sqrt(uptoNumber);

        int xSquare = 1, xStepsize = 3;

        int ySquare = 1, yStepsize = 3;

        int computedVal = 0;

        for (int x = 1; x <= squareRoot; x++) {
            ySquare = 1;
            yStepsize = 3;
            for (int y = 1; y <= squareRoot; y++) {
                computedVal = (xSquare << 2) + ySquare;

                if ((computedVal <= uptoNumber) && (computedVal % 12 == 1 || computedVal % 12 == 5))
                    isPrime.set(computedVal, !isPrime.get(computedVal));

                computedVal -= xSquare;
                if ((computedVal <= uptoNumber) && (computedVal % 12 == 7))
                    isPrime.set(computedVal, !isPrime.get(computedVal));

                if (x > y) {
                    computedVal -= ySquare << 1;
                    if ((computedVal <= uptoNumber) && (computedVal % 12 == 11))
                        isPrime.set(computedVal, !isPrime.get(computedVal));
                }
                ySquare += yStepsize;
                yStepsize += 2;
            }
            xSquare += xStepsize;
            xStepsize += 2;
        }

        for (int n = 5; n <= squareRoot; n++) {
            if (isPrime.get(n) == true) {
                int k = n * n;
                for (int z = k; z <= uptoNumber; z += k)
                    isPrime.set(z, false);
            }
        }

        for (int i = 1; i < uptoNumber; i++) {
            if (isPrime.get(i)) {
                totalCount++;
                listPrime.add(i);
            }
        }
        return (totalCount + 2); // 2 and 3 will be missed in Sieve Of Atkins
    }

    /* http://programmingpraxis.com/2010/02/19/sieve-of-atkin-improved/ */
    public static int findPrimeUsingSieveOfAtkinsOptimized(int uptoNumber) {
        int totalCount = 0;
        List<Boolean> isPrime = Arrays.asList(new Boolean[uptoNumber + 1]);

        listPrime = new ArrayList<>();
        listPrime.add(2);
        listPrime.add(3);

        Collections.fill(isPrime, Boolean.FALSE);

        int squareRoot = (int) Math.sqrt(uptoNumber);

        int xStepsize = 3;
        int y_limit = 0;
        int n = 0;

        int temp = ((int) Math.sqrt((uptoNumber - 1) / 3));
        for (int i = 0; i < 12 * temp; i += 24) {
            xStepsize += i;
            y_limit = (12 * (int) Math.sqrt(uptoNumber - xStepsize)) - 36;
            n = xStepsize + 16;
            for (int j = -12; j < y_limit + 1; j += 72) {
                n += j;
                isPrime.set(n, !isPrime.get(n));
            }

            n = xStepsize + 4;

            for (int j = 12; j < y_limit + 1; j += 72) {
                n += j;
                isPrime.set(n, !isPrime.get(n));
            }
        }

        xStepsize = 0;
        temp = 8 * (int) (Math.sqrt((uptoNumber - 1) / 4)) + 4;
        for (int i = 4; i < temp; i += 8) {
            xStepsize += i;
            n = xStepsize + 1;

            if (xStepsize % 3 != 0) {
                int tempTwo = 4 * ((int) Math.sqrt(uptoNumber - xStepsize)) - 3;
                for (int j = 0; j < tempTwo; j += 8) {
                    n += j;
                    isPrime.set(n, !isPrime.get(n));
                }

            } else {
                y_limit = 12 * (int) Math.sqrt(uptoNumber - xStepsize) - 36;
                n = xStepsize + 25;
                for (int j = -24; j < y_limit + 1; j += 72) {
                    n += j;
                    isPrime.set(n, !isPrime.get(n));
                }

                n = xStepsize + 1;

                for (int j = 24; j < y_limit + 1; j += 72) {
                    n += j;
                    isPrime.set(n, !isPrime.get(n));
                }
            }
        }

        xStepsize = 1;
        temp = (int) Math.sqrt(uptoNumber / 2) + 1;
        for (int i = 3; i < temp; i += 2) {
            xStepsize += 4 * i - 4;
            n = 3 * xStepsize;
            int s = 4;
            if (n > uptoNumber) {
                int min_y = (((int) (Math.sqrt(n - uptoNumber)) >> 2) << 2);
                int yy = min_y * min_y;
                n -= yy;
                s = 4 * min_y + 4;
            } else s = 4;

            for (int j = s; j < 4 * i; j += 8) {
                n -= j;
                if (n <= uptoNumber && n % 12 == 11)
                    isPrime.set(n, !isPrime.get(n));
            }

        }

        xStepsize = 0;
        for (int i = 2; i < temp; i += 2) {
            xStepsize += 4 * i - 4;
            n = 3 * xStepsize;
            int s = 0;
            if (n > uptoNumber) {
                int min_y = (((int) (Math.sqrt(n - uptoNumber)) >> 2) << 2) - 1;
                int yy = min_y * min_y;
                n -= yy;
                s = 4 * min_y + 4;
            } else {
                n -= 1;
                s = 0;
            }
            for (int j = s; j < 4 * i; j += 8) {
                n -= j;
                if (n <= uptoNumber && n % 12 == 11)
                    isPrime.set(n, !isPrime.get(n));
            }
        }
        for (int i = 5; i < squareRoot + 1; i += 2) {
            if (isPrime.get(i) == true) {
                int k = i * i;
                for (int z = k; z < uptoNumber; z += k)
                    isPrime.set(z, false);
            }
        }

        for (int i = 5; i < uptoNumber; i += 2) {
            if (isPrime.get(i)) {
                totalCount++;
                listPrime.add(i);
            }
        }

        return (totalCount + 2);// 2 and 3 will be missed in Sieve Of Atkins
    }

    private static int findPrimeUsingSieveOfEratosthenes(int uptoNumber) {
        int totalCount = 0;
        List<Boolean> isPrime = new ArrayList<Boolean>(Arrays.asList(new Boolean[uptoNumber + 1]));
        listPrime = new ArrayList<>();
        listPrime.add(2);
        listPrime.add(3);

        Collections.fill(isPrime, Boolean.TRUE);
        isPrime.set(0, false);
        isPrime.set(1, false);

            /* Mark all the non-primes */
        int thisFactor = 2;
        int lastSquare = 0;
        int thisSquare = 0;

        while (thisFactor * thisFactor <= uptoNumber) {
                /* Mark the multiples of this factor */
            int mark = thisFactor + thisFactor;
            while (mark <= uptoNumber) {
                isPrime.set(mark, false);
                mark += thisFactor;

            }

                /* Print the proven primes so far */
            thisSquare = thisFactor * thisFactor;
            for (; lastSquare < thisSquare; lastSquare++) {
                if (isPrime.get(lastSquare)) {
                    totalCount++;
                    listPrime.add(lastSquare);
                }

            }

                /* Set thisfactor to next prime */
            thisFactor++;
            while (!isPrime.get(thisFactor)) {
                thisFactor++;
            }

        }
            /* Print the remaining primes */
        for (; lastSquare <= uptoNumber; lastSquare++) {
            if (isPrime.get(lastSquare)) {
                totalCount++;
                listPrime.add(lastSquare);
            }

        }
        return totalCount;
    }

    private static int findPrimeUsingSieveOfSundaram(int uptoNumber) {
        int totalCount = 0;
        int k = uptoNumber / 2;
        List<Boolean> isPrime = new ArrayList<Boolean>(Arrays.asList(new Boolean[uptoNumber + 1]));
        listPrime = new ArrayList<>();
        listPrime.add(2);

        Collections.fill(isPrime, Boolean.TRUE);

            /* SEIVE */
        int maxVal = 0;
        int denominator = 0;
        for (int i = 1; i < k; i++) {
            denominator = (i << 1) + 1;
            maxVal = (k - i) / denominator;
            for (int j = i; j <= maxVal; j++) {
                isPrime.set(i + j * denominator, false);
            }
        }

        for (int i = 1; i < k; i++)
        {
            if (isPrime.get(i))
            {
                totalCount++;
                listPrime.add((i << 1) + 1);
            }
        }
        return (totalCount + 1); // 2 will be
    }
}
