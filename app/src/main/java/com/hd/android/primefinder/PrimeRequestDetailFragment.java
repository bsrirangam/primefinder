package com.hd.android.primefinder;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hd.android.primefinder.adapter.CustomAdapter;
import com.hd.android.primefinder.util.PrimeCalculator;
import com.hd.android.primefinder.util.Utils;

import java.util.List;

/**
 * A fragment representing a single PrimeRequest detail screen.
 * This fragment is either contained in a {@link PrimeRequestMainActivity}
 * in two-pane mode (on tablets) or a {@link PrimeRequestDetailActivity}
 * on handsets.
 */
public class PrimeRequestDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
//    public static final String ARG_ITEM_ID = "item_id";
    public static final String PRIME_LIST_ID = "prime_list_id";
    public static final String NTH_PRIME_ID = "nth_prime_id";
    public static final String SIEVE_SELECTED = "sieve_selected";

    private double duration;
    private int results;
    private ProgressDialog dialog;
    private RecyclerView recyclerView;
    private TextView resultTextView;
    private List<Integer> subList;
    private int prime;
    private int sieve;
    private String resultString;
    private Context context;
    private LinearLayout prime_index_detail_layout;

    public PrimeRequestDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new ProgressDialog(getActivity());
        context = getActivity();
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.primerequest_detail, container, false);
        resultTextView = ((TextView) rootView.findViewById(R.id.primerequest_detail));
        recyclerView = (RecyclerView) rootView.findViewById(R.id.primerequest_list);
        prime_index_detail_layout = (LinearLayout) rootView.findViewById(R.id.prime_index_detail_layout);
        recyclerView.setNestedScrollingEnabled(false);

        if (isPrimeList()) {
            prime = getArguments().getInt(PRIME_LIST_ID);
            sieve = getArguments().getInt(SIEVE_SELECTED);
            computeAndShowPrimeList();


        } else if (isPrimeSearch()) {
            prime = getArguments().getInt(NTH_PRIME_ID);
            sieve = getArguments().getInt(SIEVE_SELECTED);
            computeAndShowPrimeNumber();
        }

        return rootView;
    }

    private void computeAndShowPrimeNumber() {
        new AsyncTask() {
            long startTime;
            long endTime;

            @Override
            protected Object doInBackground(Object[] params) {
                results = PrimeCalculator.findNthPrime(prime, sieve);
                return null;
            }

            @Override
            protected void onPreExecute() {
                dialog.setTitle(getActivity().getString(R.string.computing_nth_prime));
                dialog.setMessage(getActivity().getString(R.string.loading));
                dialog.show();
                startTime = System.nanoTime();
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Object o) {
                dialog.dismiss();
                if (results != -1) {
                    updateUI();
                } else {
                    prime_index_detail_layout.setVisibility(View.GONE);
                    AlertDialog alert = Utils.showAllert(getString(R.string.computation_error), context);
                    alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if(getActivity() instanceof PrimeRequestDetailActivity) {
                                getActivity().finish();
                            }
                        }
                    });
                }
                endTime = System.nanoTime();
                duration = (double) (endTime - startTime) / 1000000;
                super.onPostExecute(o);
            }
        }.execute();
    }

    private void computeAndShowPrimeList() {
        new AsyncTask() {
            long startTime;
            long endTime;

            @Override
            protected Object doInBackground(Object[] params) {
                results = PrimeCalculator.findPrimeTillN(prime, sieve);
                return null;
            }

            @Override
            protected void onPreExecute() {
                dialog.setTitle(getActivity().getString(R.string.computing_primes));
                dialog.setMessage(getActivity().getString(R.string.loading));
                dialog.show();
                startTime = System.nanoTime();
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(Object o) {
                endTime = System.nanoTime();
                duration = (double) (endTime - startTime) / 1000000;
                dialog.dismiss();
                if (results != -1) {
                    updateUI();
                } else {
                    prime_index_detail_layout.setVisibility(View.GONE);
                    AlertDialog alert = Utils.showAllert(getString(R.string.computation_error), context);
                    alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if(getActivity() instanceof PrimeRequestDetailActivity) {
                                getActivity().finish();
                            }
                        }
                    });
                }
                super.onPostExecute(o);
            }
        }.execute();
    }

    public void updateUI() {
        subList = null;

        if (isPrimeList()) {
            if (results <= PrimeCalculator.listPrime.size()) {
                subList = PrimeCalculator.listPrime.subList(0, results);
            } else {
                subList = PrimeCalculator.listPrime;
            }
            resultTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            prime_index_detail_layout.setVisibility(View.VISIBLE);
            resultString = "Time taken to compute: " + duration + " ms"
                    + "\nPrimes Number Count : " + results;
            resultTextView.setText(resultString);
            recyclerView.setAdapter(new CustomAdapter(subList));
        } else if (isPrimeSearch()) {
            String title = getTitleForNumber(getArguments().getInt(NTH_PRIME_ID));
            resultString = title + " prime number is: " + results;
            resultTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            prime_index_detail_layout.setVisibility(View.GONE);
            resultTextView.setText(resultString);
        }

    }


    private String getTitleForNumber(int prime) {
        String[] sufixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        switch (prime % 100) {
            case 11:
            case 12:
            case 13:
                return prime + "th";
            default:
                return prime + sufixes[prime % 10];

        }
    }

    public boolean isPrimeList() {
        return getArguments().containsKey(PRIME_LIST_ID) && getArguments().getInt(PRIME_LIST_ID) != 0;
    }

    public boolean isPrimeSearch() {
        return getArguments().containsKey(NTH_PRIME_ID) && getArguments().getInt(NTH_PRIME_ID) != 0;
    }


}
