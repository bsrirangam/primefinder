package com.hd.android.primefinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;

/**
 * An activity representing a single PrimeRequest detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link PrimeRequestMainActivity}.
 */
public class PrimeRequestDetailActivity extends AppCompatActivity {
    private static final int CONTENT_VIEW_ID = 10101010;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout frame = new FrameLayout(this);
        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
//            arguments.putString(PrimeRequestDetailFragment.ARG_ITEM_ID,
//                    getIntent().getStringExtra(PrimeRequestDetailFragment.ARG_ITEM_ID));

            arguments.putInt(PrimeRequestDetailFragment.PRIME_LIST_ID,
                    getIntent().getIntExtra(PrimeRequestDetailFragment.PRIME_LIST_ID, 0));

            arguments.putInt(PrimeRequestDetailFragment.NTH_PRIME_ID,
                    getIntent().getIntExtra(PrimeRequestDetailFragment.NTH_PRIME_ID, 0));

            arguments.putInt(PrimeRequestDetailFragment.SIEVE_SELECTED,
                    getIntent().getIntExtra(PrimeRequestDetailFragment.SIEVE_SELECTED, 0));

            PrimeRequestDetailFragment fragment = new PrimeRequestDetailFragment();
            fragment.setArguments(arguments);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(CONTENT_VIEW_ID, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            navigateUpTo(new Intent(this, PrimeRequestMainActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
